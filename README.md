# friendsofsymfony-rest-bundle

RESTful API's. [friendsofsymfony/rest-bundle](https://packagist.org/packages/friendsofsymfony/rest-bundle)

# Deconstruction
* [*Another way for a Symfony API to ingest POST requests - No Bundle*
  ](https://dev.to/gmorel/another-way-for-a-symfony-api-to-ingest-post-requests-no-bundle-5h15)
  2020-07 Guillaume MOREL

